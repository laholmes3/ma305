#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Template for Python file, Levi Holmes.
"""
__version__ = "0.0.1"
__author__ = "Levi Holmes"
__license__ = "GPL 3.0"

import sys
import os, os.path
import re, string
import traceback, pprint
import argparse
import time
import math
import numpy as np
import csv

# Global variables spare me from putting them in the call of each fcn.
VERBOSE = False
DEBUG = False

PGM_ROOTNAME = os.path.splitext(os.path.basename(sys.argv[0]))[0]
PGM_SRC_DIR = os.path.dirname(__file__)

class JHException(Exception):
    pass

import logging
# Potential logging levels: DEBUG | INFO | WARNING | ERROR | CRITICAL
LOG_LEVEL_CHOICES=["debug", "info", "warning", "error", "critical", "default"]
DEFAULT_LOG_LEVEL = "warning"
if not(DEFAULT_LOG_LEVEL in LOG_LEVEL_CHOICES):
    critical("DEFAULT_LOG_LEVEL "+str(DEFAULT_LOG_LEVEL)+ \
             " must be in LOG_LEVEL_CHOICES="+str(LOG_LEVEL_CHOICES))

def _set_log_level(log, choice=DEFAULT_LOG_LEVEL):
    c = choice.casefold()  # like lower() but for case-insensitive matching
    # log.debug('Logging level set to '+choice)
    if (c == "debug"):
        log.setLevel(logging.DEBUG)
    elif (c == "info"):
        log.setLevel(logging.INFO)
    elif (c == "warning"):
        log.setLevel(logging.WARNING)
    elif (c == "error"):
        log.setLevel(logging.ERROR)
    elif (c == "critical"):
        log.setLevel(logging.CRITICAL)
    else:
        error("Logging level {0!s} not known".format(choice))

# Establish logging
log = logging.getLogger(__name__)
_set_log_level(log)
# Log errors to the console
log_sh = logging.StreamHandler(stream=sys.stderr)
log_sh.setFormatter(logging.Formatter('%(levelname)s - Line: %(lineno)d\n  %(message)s'))
_set_log_level(log_sh,"ERROR")
log.addHandler(log_sh)
# Log most everything to a file
log_fh = logging.FileHandler(os.path.abspath(os.path.join(
    os.path.dirname(__file__), 
    os.path.basename(__file__).rstrip('.py') + '.log')),
                         mode='w')
log_fh.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - Line: %(lineno)d\n%(message)s'))
_set_log_level(log_fh,"INFO")
log.addHandler(log_fh)
if DEBUG:
    _set_log_level(log_sh,"DEBUG")
    _set_log_level(log_fh,"DEBUG")

def warning(s):
    t = 'WARNING: '+s+"\n"
    log.warning(t)

def error(s, level=10):
    t = 'ERROR: '+s+"\n"
    log.error(t,exc_info=True)
    sys.exit(level)

def critical(s, level=1):
    t = 'CRITICAL ERROR: '+s+"\n"
    log.critical(t,exc_info=True)
    sys.exit(level)


# ===========================================================
def pivot(mat, a, b, r):
    """Pivot a matrix row
    mat is a 2-dimensional numpy matrix
    r is the pivot factor
    a is a non-negative integer which serves as the index of a row
    b is a non-negative integer which serves as the index of a row"""  
    for i in range(mat.shape[1]):
        mat[b,i] = r*mat[a,i]+mat[b,i]
    return(mat)

def swap(mat,a,b):
    """Swap 2 rows in a matrix
    mat is a 2-dimensional numpy matrix
    a is a non-negative integer which serves as the index of a row
    b is a non-negative integer which serves as the index of a row"""  
    for i in range(mat.shape[1]):
        c=mat[b,i]
        mat[b,i] = mat[a,i]
        mat[a,i] = c
    return(mat)

def rescale(mat,a,r):
    """Rescale a matrix row
    mat is a 2-dimensional numpy matrix
    a is a non-negative integer which serves as the index of a row
    r is the scaling factor"""  
    for i in range(mat.shape[1]):
        mat[a,i] = r*mat[a,i]
    return(mat)
        
def gaussmethod(mat):
    (m,n) = mat.shape
    print(mat)
    for k in range(0, m):
        for i in range(k+1, m):
            leading_entry = mat[k,k]
            entry_to_change=mat[i,k]
            print(mat)
            pivot(mat,k,1,-1*(entry_to_change/leading_entry))
            #entry = mat[i][k]
            #for j in range(0, n):
                #mat[i][j]=mat[i][j]-(entry/mat[k][k]) * mat[k][j]
            print(mat)
    return(mat)

            
def main(args):
    #matrix = np.random.random((3, 3))
    matrix = np.array([[1,2,3], [4,5,6], [7,8,10]])
    with open('matrix.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quoting = csv.QUOTE_MINIMAL)
        spamwriter.writerows(matrix)
        
    with open('matrix.csv', newline='') as f:
        spamreader=csv.reader(f, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
        A =[]
        for row in spamreader:
            A.append(row)
        mat=np.array(A)

    gaussmethod(mat)


# ===========================================================
if __name__ == '__main__':
    try:
        start_time = time.time()
        # Parser: See http://docs.python.org/dev/library/argparse.html
        parser = argparse.ArgumentParser(description=__doc__+
                                         "  Author: "+__author__
                                         +", Version: "+__version__
                                         +", License: "+__license__)
        parser.add_argument('-D', '--debug',
                            action='store_true',
                            default=DEBUG,
                            help="Run debugging code. Default: {0!s}".format(DEBUG))
        parser.add_argument('-L', '--log_level',
                            action='store',
                            type=str,
                            choices=LOG_LEVEL_CHOICES,
                            default=DEFAULT_LOG_LEVEL,
                            help="Set the logging level. Default: {0!s}".format(DEFAULT_LOG_LEVEL))
        parser.add_argument('-v', '--version',
                            action='version',
                            version=__version__)
        parser.add_argument('-V', '--verbose',
                            action='store_true',
                            default=False,
                            help='Give verbose output. Default: {0!s}'.format(VERBOSE))
        log.info("{0!s} Started".format(parser.prog))
        args = parser.parse_args()
        _set_log_level(log,args.log_level)        
        if args.debug:
            _set_log_level(log_fh,"DEBUG")
            _set_log_level(log,"DEBUG")
        elif args.verbose:
            _set_log_level(log_fh,"INFO")
            _set_log_level(log,"INFO")
        main(args)
        _set_log_level(log,"INFO")
        log.info("{0!s} Ended.  Elapsed time {1:0.2f} sec".format(parser.prog,time.time() - start_time))
        sys.exit(0)
    except KeyboardInterrupt as e:  # Ctrl-C
        raise e
    except SystemExit as e:  # sys.exit()
        raise e
    except Exception as e:
        print('ERROR, UNEXPECTED EXCEPTION')
        print(str(e))
        traceback.print_exc()
        os._exit(1)
